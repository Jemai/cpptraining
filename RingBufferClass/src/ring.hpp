/*
 * ring.hpp
 *
 *  Created on: Dec 5, 2019
 *      Author: renault
 */

#ifndef RING_HPP_
#define RING_HPP_

#include <iostream>
using namespace std;

template<class T>
class ring{
private:
	int m_size;
	int m_pos;
	T *m_values;

public:
	class iterator;

public:
	ring(int size):m_pos(0),m_size(size), m_values(NULL){
		m_values = new T[size];
	}

	~ring(){
		delete[] m_values;
	}

	int size(){
		return m_size;
	}

	void add(T value){
        m_values[m_pos++] = value;
        //m_pos++;
        if(m_pos == m_size){
        	m_pos = 0;
        }
	}
     T get(int i){
    	 return (m_values[i%m_size]);
     }

};

template<class T>
class ring<T>::iterator {
public:
	void print(){
		cout << "Hello from iterator: "<<T()<<endl;
	}
};


#endif /* RING_HPP_ */
