//============================================================================
// Name        : RingBufferClass.cpp
// Author      : Nabil JEMAI
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "ring.hpp"
using namespace std;

int main() {
	cout << "!!!Hello World!!!" << endl; // prints !!!Hello World!!!
	ring<string> textring(3);
	textring.add("one");
	textring.add("two");
	textring.add("three");
	textring.add("four");

	for(int i=0;i<textring.size();i++){
		cout<<i<<"- "<<textring.get(i)<< endl;
	}

	return 0;
}
